import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private userRoles: string[];

  readonly ADMIN_ROLE = 'ADMIN_ROLE';
  readonly OPERATOR_ROLE = 'OPERATOR_ROLE';
  readonly CUSTOMER_ROLE = 'CUSTOMER_ROLE';

  constructor(private router: Router, private authService: AuthService) {
    this.checkEnteredUserExistingLocalStorage();
  }

  private checkEnteredUserExistingLocalStorage() {
    if (this.authService.getUserRoles() != null) {
      this.userRoles = this.authService.getUserRoles();

      if (
        this.userRoles.includes(this.ADMIN_ROLE) ||
        this.userRoles.includes(this.OPERATOR_ROLE)
      ) {
        this.router.navigate(['management']);
      } else {
        this.router.navigate(['client']);
      }
    }
  }
}
