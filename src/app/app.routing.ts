import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './core/components/home/home.component';
import { AuthGuard } from './core/guards/auth.guard';
import { NotFoundPageComponent } from './core/components/not-found-page/not-found-page.component';

const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule' },
  {
    path: 'client',
    loadChildren: 'app/client/client.module#ClientModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'management',
    loadChildren: 'app/management/management.module#ManagementModule',
    canActivate: [AuthGuard],
  },

  {
    path: 'order',
    loadChildren: 'app/order/order.module#OrderModule',
  },

  { path: 'beacon-activation', redirectTo: 'management/beacon-activation' },
  { path: 'beacon-shipment', redirectTo: 'management/beacon-shipment' },
  {
    path: 'beacon-change-message',
    redirectTo: 'management/beacon-change-message',
  },
  { path: 'affiliate-program', redirectTo: 'client/affiliate-program' },
  { path: 'ask-question', redirectTo: 'client/ask-question' },
  { path: 'gems', redirectTo: 'client/gems' },
  { path: 'order/success', redirectTo: 'auth/order/success' },
  { path: 'landing-page', redirectTo: 'client/landing-page' },
  { path: 'users-list', redirectTo: 'management/users-list' },
  { path: 'adjustments-list', redirectTo: 'management/adjustments-list' },
  { path: '404', component: NotFoundPageComponent },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
