import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label?: string;
  main: MainMenuItems[];
}

const ManagerMenuItems = [
  {
    main: [
      {
        state: 'management',
        name: 'Beacon Operations',
        type: 'sub',
        icon: 'fa fa-bar-chart',
        children: [
          {
            state: 'beacon-search',
            name: 'Beacon Search',
          },
        ],
      },
      {
        state: 'management',
        name: 'Projects',
        type: 'sub',
        icon: 'ti-layout-grid2-alt',
        children: [
          {
            state: 'projects-list',
            name: 'List of projects',
          },
          {
            state: 'create-project',
            name: 'Create Project',
          },
          {
            state: 'tabs',
            name: 'Create account',
          },
        ],
      },
      {
        state: 'users-list',
        name: 'Users Management',
        type: 'link',
        icon: 'fa fa-users',
      },
      {
        state: 'advance',
        name: 'Beacons movement',
        type: 'link',
        icon: 'fa fa-arrows',
      },
      {
        state: 'beacon-activation',
        name: 'Activate Beacon',
        type: 'link',
        icon: 'fa fa-check',
      },
      {
        state: 'beacon-change-message',
        name: 'Change request',
        type: 'link',
        icon: 'fa fa-commenting-o',
      },
      {
        state: 'beacon-shipment',
        name: 'Beacon Shipment',
        type: 'link',
        icon: 'fa fa-truck',
      },
    ],
  },
];

const ClientMenuItems = [
  {
    main: [
      {
        state: 'gems',
        name: 'My gems',
        type: 'link',
        icon: 'fa fa-cubes',
      },
      {
        name: 'Account Billing',
        type: 'link',
        icon: 'fa fa-credit-card',
      },
      {
        state: 'affiliate-program',
        name: 'Commissions',
        type: 'link',
        icon: 'fa fa-bar-chart',
      },
      /*{
        state: 'ask-question',
        name: 'Ask question',
        type: 'link',
        icon: 'fa fa-commenting-o',
      },*/
    ],
  },
];

@Injectable()
export class MenuItems {
  getManagerMenu(): Menu[] {
    return ManagerMenuItems;
  }

  getClientMenu(): Menu[] {
    if (
      !ClientMenuItems[0].main.some(item => item.name === 'Links & Materials')
    ) {
      const user = localStorage.getItem('currentUser');
      if (
        user &&
        JSON.parse(user).customer &&
        JSON.parse(user).customer.affiliateCode
      ) {
        const item: any = {
          state: 'landing-page',
          name: 'Links & Materials',
          type: 'link',
          icon: 'fa fa-link',
        };
        ClientMenuItems[0].main.push(item);
      }
    }
    return ClientMenuItems;
  }
}
