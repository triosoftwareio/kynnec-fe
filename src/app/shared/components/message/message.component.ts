import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';

@Component({
  selector: 'app-gems-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent {
  @Input() chosenGem: any;
  @Input() weekDays: string[];
  @Input() submit: boolean;
  @Input() urlError: any;
  @Output() changeInputData: EventEmitter<object> = new EventEmitter<object>();

  changeInputValue(e, field, cbIndex) {
    this.changeInputData.emit({ e, field, cbIndex });
  }

  constructor() {}
}
