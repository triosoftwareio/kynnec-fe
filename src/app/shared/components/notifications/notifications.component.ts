import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-notifications',
  template: `
    <ng2-toasty [position]="position"></ng2-toasty>`,
})
export class NotificationsComponent implements OnInit {
  position = 'top-right';
  title: string;
  showClose = true;
  timeout = 5000;
  theme = 'bootstrap';
  type = 'default';

  constructor(
    private toastyService: ToastyService,
    private notificationService: NotificationService
  ) {
    this.notificationService.toastyLoaderEventEmmiter.subscribe((data: any) => {
      this.addToast(data);
    });
  }

  ngOnInit() {}

  addToast(options) {
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: this.timeout,
      theme: this.theme,
    };

    switch (options.type) {
      case 'default':
        this.toastyService.default(toastOptions);
        break;
      case 'info':
        this.toastyService.info(toastOptions);
        break;
      case 'success':
        this.toastyService.success(toastOptions);
        break;
      case 'wait':
        this.toastyService.wait(toastOptions);
        break;
      case 'error':
        this.toastyService.error(toastOptions);
        break;
      case 'warning':
        this.toastyService.warning(toastOptions);
        break;
    }
  }
}

// html part

// <button class="btn btn-success waves-effect" (click)="onToast({title:'www', msg:'Test Notification', timeout: 5000, theme:'material', position:'bottom-right', type:'success'})">Notfication test</button>

//component part

// import { NotificationService } from '../../../core/services/notification.service';

// constructor(private notificationService: NotificationService){}

// onToast(object) {
//     this.notificationService.showToasty(object);
//   }
