import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItems } from './components/menu-items/menu-items';
import { AccordionAnchorDirective } from './directives/accordion/accordionanchor.directive';
import { AccordionLinkDirective } from './directives/accordion/accordionlink.directive';
import { AccordionDirective } from './directives/accordion/accordion.directive';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { TitleComponent } from './components/title/title.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from './components/table/table.component';
import { CardComponent } from './components/card/card.component';
import { DataTableModule } from 'angular2-datatable';
import { GoTopButtonModule } from 'ng2-go-top-button';
import { MessageComponent } from './components/message/message.component';
import { CardRefreshDirective } from './components/card/card-refresh.directive';
import { CardToggleDirective } from './components/card/card-toggle.directive';
import { LoadingModule } from 'ngx-loading';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastyModule } from 'ng2-toasty';
import { NotificationsComponent } from './components/notifications/notifications.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    DataTableModule,
    GoTopButtonModule,
    ReactiveFormsModule,
    LoadingModule,
    NgxDatatableModule,
    ToastyModule.forRoot(),
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent,
    BreadcrumbsComponent,
    TitleComponent,
    RouterModule,
    FormsModule,
    TableComponent,
    CardComponent,
    DataTableModule,
    GoTopButtonModule,
    ReactiveFormsModule,
    MessageComponent,
    CardRefreshDirective,
    CardToggleDirective,
    LoadingModule,
    NgxDatatableModule,
    ToastyModule,
    NotificationsComponent,
  ],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent,
    BreadcrumbsComponent,
    TitleComponent,
    TableComponent,
    CardComponent,
    MessageComponent,
    CardRefreshDirective,
    CardToggleDirective,
    NotificationsComponent,
  ],
  providers: [MenuItems],
})
export class AppSharedModule {}
