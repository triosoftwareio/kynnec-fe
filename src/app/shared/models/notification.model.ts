interface INotificationModel {
  msg: string;
  type: string;
}

export class NotificationModel implements INotificationModel {
  msg: string;
  type: string;

  constructor(msg: string, type: string) {
    this.msg = msg;
    this.type = type;
  }
}
