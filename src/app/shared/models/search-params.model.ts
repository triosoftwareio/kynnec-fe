export interface SearchParams {
  keyword: string;
  sort: string;
  skip: number;
  limit: number;
}
