export interface Customer {
  address: string;
  affiliate_id: string;
  city: string;
  color: string;
  contactType: string;
  country: string;
  customer: Customer;
  dateCreated: any;
  email: string;
  emailClicks: number;
  emailOpens: number;
  firstName: string;
  gemOrderStatus: string;
  lastName: string;
  name: string;
  mdc_id: string;
  referredBy: string;
  state: string;
  tagIds: string;
  tags: string;
  views: number;
  visits: number;
  zip: string;
  _id: string;
  homePhone: string;
  login: string;
}
