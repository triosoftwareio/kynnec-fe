export interface Attachments {
  attachmentName: string;
  namespacedType: string;
  notificationMessage: NotificationMessage;
}
