import { Attachments } from './attachments.model';
import { Customer } from './customer.model';

export interface ChosenGem {
  _id?: string;
  beaconName?: string;
  attachments: Attachments[];
  status: boolean;
  customer: Customer;
}
