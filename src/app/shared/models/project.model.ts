import { Account } from './account.model';

export interface Project {
  projectId: string;
  account: Account;
}
