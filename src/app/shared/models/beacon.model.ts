import { Customer } from './customer.model';
import { Project } from './project.model';
import { Attachments } from './attachments.model';
import { User } from '../../management/models/user.model';

export interface Beacon {
  _id?: string;
  createdOn?: string;
  createdBy?: string;
  updatedOn?: string;
  updatedBy?: string;
  beaconName?: string;
  customer?: Customer;
  user?: User;
  project?: Project;
  attachments?: Attachments[];
  attachmentsRequest?: Attachments[];
  status?: string;
  orderStatus?: string;
  description?: string;
}
