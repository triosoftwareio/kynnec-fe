interface NotificationMessage {
  title: string;
  url: string;
  days: boolean[];
}
