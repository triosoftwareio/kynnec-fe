import { Customer } from './customer.model';

export class UserStorageModel {
  login: string;
  customer: Customer;
  roles: string[];
  token: string;
}
