import { DownlineModel } from './downline.model';
import { UserReportModel } from './user-report.model';

export interface AffiliateModel {
  getUserReport: UserReportModel;
  userDownline: UserReportModel;
}
