interface IReport {
  cash_bonus_amount: number;
  gem_level: string;
  monthly_earnings: number;
  monthly_revenue: number;
  users_sponsored: number;
}

export class ReportModel implements IReport {
  cash_bonus_amount = 0;
  gem_level = '';
  monthly_earnings = 0;
  monthly_revenue = 0;
  users_sponsored = 0;
}
