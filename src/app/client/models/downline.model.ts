export interface DownlineModel {
  children: DownlineModel[];
  email: string;
  id: number;
  monthly_revenue: number;
  name: string;
  status: string;
  subscription_amount: any;
  timestamp: string;
  firstLevelVolume;
  secondLevelVolume;
}
