import { DownlineModel } from './downline.model';
import { ReportModel } from './report.model';

export interface UserReportModel {
  downline: DownlineModel;
  message: string;
  status: string;
  report: ReportModel;
}
