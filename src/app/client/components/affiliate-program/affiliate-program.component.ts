import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';
import { UserService } from '../../../core/services/user.service';
import { DownlineModel } from '../../models/downline.model';
import { AffiliateModel } from '../../models/affiliate.model';
import { UserReportModel } from '../../models/user-report.model';
import { AuthService } from '../../../core/services/auth.service';
import { ReportModel } from '../../models/report.model';
import * as moment from 'moment';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-affiliate-program',
  templateUrl: './affiliate-program.component.html',
  styleUrls: ['./affiliate-program.component.scss'],
})
export class AffiliateProgramComponent implements OnInit {
  // commissionData: UserReportModel;
  commissionHeader: ReportModel = new ReportModel();

  @ViewChild('myTable') table: any;

  children: DownlineModel[] = [];
  innerChildrens: DownlineModel[] = [];
  loading: boolean;
  private currentUserEmail: string;
  firstLevelVolumePayment: number;
  secondLevelVolumePayment: number;
  child: DownlineModel;

  selectedMonth = '10';
  payoutEndDate = '2017-10-31 23:59:59';
  cashBonusStartDate = '2017-10-01 00:00:00';
  cashBonusEndDate = '2017-10-31 23:59:59';

  rows: any[] = [];
  expanded: any = {};
  timeout: any;

  nextDay: Date;

  private notificationObject;

  constructor(
    private rest: RestService,
    private authService: AuthService,
    private userService: UserService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.getCommission();
  }

  onMonthChange() {
    //   const myMoment: moment.Moment = moment("someDate");
    /*  const date = new Date();
    const y = date.getFullYear();
    const m = date.getMonth();
    console.log('month', m);

    var firstDay = new Date(y, m, 1, 0, 0, 1);
    var lastDay = moment(firstDay).add(1, 'months');

     var firstDayResult = moment(firstDay).format('YYYY-MM-DD HH:MM:SS');
     var lastDayResult = moment(lastDay).format('YYYY-MM-DD HH:MM:SS');

    console.log('firstDay', firstDayResult);
    console.log('lastDat', lastDayResult);*/

    switch (this.selectedMonth) {
      case '7':
        this.payoutEndDate = '2017-07-31 23:59:59';
        this.cashBonusStartDate = '2017-07-01 00:00:00';
        this.cashBonusEndDate = '2017-07-31 23:59:59';
        break;
      case '8':
        this.payoutEndDate = '2017-08-31 23:59:59';
        this.cashBonusStartDate = '2017-08-01 00:00:00';
        this.cashBonusEndDate = '2017-08-31 23:59:59';
        break;
      case '9':
        this.payoutEndDate = '2017-09-30 23:59:59';
        this.cashBonusStartDate = '2017-09-01 00:00:00';
        this.cashBonusEndDate = '2017-09:30 23:59:59';
        break;
      case '10':
        this.payoutEndDate = '2017-10-31 23:59:59';
        this.cashBonusStartDate = '2017-10-01 00:00:00';
        this.cashBonusEndDate = '2017-10-31 23:59:59';
        break;
    }

    this.getCommission();
  }

  private getCommission() {
    this.currentUserEmail = this.userService.getCurrentUserEmail();

    this.loading = true;

    this.rest
      .commission()
      .stats.show({
        email: this.currentUserEmail,
        payoutEndDate: this.payoutEndDate,
        cashBonusStartDate: this.cashBonusStartDate,
        cashBonusEndDate: this.cashBonusEndDate,
      })
      .subscribe(
        (response: any) => {
          if (response.status === 200) {
            if (response.data.content.getUserReport.report != null) {
              this.commissionHeader =
                response.data.content.getUserReport.report;
            }

            if (response.data.content.userDownline.downline != null) {
              if (
                response.data.content.userDownline.downline.children != null
              ) {
                this.children = Object.values(
                  response.data.content.userDownline.downline.children
                );
              }
            }
          }
          this.loading = false;
        },
        error => {
          this.notificationObject = new NotificationModel(
            'Affiliate error loading occurred!',
            'error'
          );
          this.onToast();
          this.loading = false;
        }
      );
  }

  private calculateFirstLevelRevenue() {
    let sum = 0;
    let i = 0;
    for (const child of this.children) {
      for (const firstLevelRevenue of child.children) {
        sum += firstLevelRevenue.monthly_revenue;
      }
      child.firstLevelVolume = sum;
      sum = 0;
      this.children[i] = child;
      i++;
    }
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {}, 100);
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    this.innerChildrens = Object.values(event.value.children);
  }

  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
