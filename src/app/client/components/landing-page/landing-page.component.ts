import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  loading = false;
  private notificationObject;
  submit = false;
  newAffiliateCode: '';
  user: any = {
    firstName: '',
    affiliateCode: '',
  };
  constructor(
    private userService: UserService,
    private rest: RestService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    const user = this.userService.getUser();
    this.user.firstName = user.customer.firstName;
    this.user.affiliateCode = user.customer.affiliateCode;
  }
  returnName() {
    return `Welcome ${this.user.firstName},`;
  }
  returnFirstLink() {
    return `https://www.royaltie.com/?af=${this.user.affiliateCode}`;
  }
  returnSecondLink() {
    return `https://app.royaltie.com/order/gems?af=${this.user.affiliateCode}`;
  }
  changeCodeValue(e) {
    this.submit = false;
    this.newAffiliateCode = e.target.value;
  }
  saveNewAffiliateCode() {
    this.submit = true;
    this.loading = true;
    if (!this.newAffiliateCode) {
      return;
    }

    this.rest
      .client()
      .changeAffiliateCode({ affiliateCode: this.newAffiliateCode })
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.loading = false;
            const prevUserData: any = this.userService.getUser();
            prevUserData.customer = response.data.content;
            this.userService.saveUser(prevUserData);
            this.notificationObject = new NotificationModel(
              response.data.message,
              'success'
            );
            this.onToast();
            this.user.affiliateCode = this.newAffiliateCode;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            error.message,
            'error'
          );
          this.onToast();
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
