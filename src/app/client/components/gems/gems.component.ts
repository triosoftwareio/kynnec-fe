import { Component, OnChanges, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { UserService } from '../../../core/services/user.service';
import { ResponseModel } from '../../../core/models/response.model';
import { Beacon } from '../../../shared/models/beacon.model';
import { SearchService } from '../../../core/services/search.service';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

interface Params {
  keyword: string;
}

@Component({
  selector: 'app-gems',
  templateUrl: './gems.component.html',
  styleUrls: ['./gems.component.scss'],
})
export class GemsComponent implements OnInit {
  loading: boolean;
  searchModel: string;
  searchParams: Params = { keyword: '' };
  gems: Beacon[] = [];
  chosenGem: Beacon = null;
  weekDays: string[] = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];
  submit = false;
  urlRule = /^(https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  urlError = null;
  private notificationObject;

  constructor(
    private rest: RestService,
    private userService: UserService,
    private searchService: SearchService,
    private notificationService: NotificationService
  ) {
    this.loading = false;
    searchService.changeModel(model => {
      this.searchParams.keyword = model;
      this.getGems();
    });
  }

  ngOnInit() {
    this.checkForUnassignedBeacons();
  }

  checkForUnassignedBeacons() {
    const user = this.userService.getUser();
    if (user && user.customer && user.customer.unassignedBeacons) {
      for (let i = 0; i < user.customer.unassignedBeacons; i++) {
        this.gems.push({
          description: 'Beacon Ordered',
        });
      }
    }
    this.getGems();
  }

  getGems() {
    this.loading = true;
    this.rest
      .client()
      .beacon.list(this.searchParams)
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.gems = this.gems.concat(response.data.content.data);
            if (this.gems.length) {
              this.chooseBeacon();
            } else {
              this.chosenGem = null;
            }
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
          this.chosenGem = null;
          this.gems = [];
        }
      );
  }

  chooseBeacon() {
    this.gems.forEach(beacon => {
      if (beacon._id) {
        if (!this.chosenGem) {
          this.chosenGem = beacon;
        }
      }
    });
  }

  returnBeaconName(name) {
    if (!name) {
      return '';
    }
    if (name !== 'Beacon Ordered') {
      return `Beacon ${name.slice(-4)}`;
    }
    return name;
  }

  search(text: string) {
    this.searchService.nextValue(text);
  }

  changeChosenGem(i) {
    if (this.gems[i]._id) {
      this.chosenGem = this.gems[i];
    }
  }

  onChangeInputData(data): void {
    if (data.field !== 'checkbox') {
      if (data.field === 'url') {
        this.submit = false;
        this.urlError = null;
      }
      this.chosenGem.attachments[0].notificationMessage[data.field] =
        data.e.target.value;
    } else {
      this.chosenGem.attachments[0].notificationMessage.days[data.cbIndex] =
        data.e.target.checked;
    }
  }

  saveGem() {
    this.submit = true;
    const url = this.chosenGem.attachments[0].notificationMessage.url;
    if (!this.urlRule.test(url)) {
      this.urlError = `Url is not valid or does not start with https://`;
      return;
    }
    const arr = this.chosenGem.attachments[0].attachmentName.split('/');
    const attachmentName = arr[arr.length - 1];
    this.loading = true;
    this.rest
      .client()
      .beacon.update(this.chosenGem._id, attachmentName, {
        attachment: this.chosenGem.attachments[0].notificationMessage,
      })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Beacon saved',
              'success'
            );
            this.onToast();
            this.chosenGem = response.data.content;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
