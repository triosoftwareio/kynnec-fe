import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';

@Component({
  selector: 'app-account-billing',
  templateUrl: './account-billing.component.html',
  styleUrls: ['./account-billing.component.scss'],
})
export class AccountBillingComponent implements OnInit {
  constructor(private rest: RestService) {}

  ngOnInit() {}
}
