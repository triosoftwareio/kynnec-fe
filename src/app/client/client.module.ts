import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ClientComponent } from './client.component';
import { GemsComponent } from './components/gems/gems.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AccountBillingComponent } from './components/account-billing/account-billing.component';
import { AffiliateProgramComponent } from './components/affiliate-program/affiliate-program.component';
import { AskQuestionComponent } from './components/ask-question/ask-question.component';
import { ClientRouting } from './client.routing';
import { AppSharedModule } from '../shared/app-shared.module';

@NgModule({
  imports: [CommonModule, ClientRouting, RouterModule, AppSharedModule],
  declarations: [
    ClientComponent,
    GemsComponent,
    AccountBillingComponent,
    AffiliateProgramComponent,
    AskQuestionComponent,
    LandingPageComponent,
  ],
})
export class ClientModule {}
