import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MenuItems } from '../shared/components/menu-items/menu-items';
import { AuthService } from '../core/services/auth.service';
import { Router } from '@angular/router';
import { RestService } from '../core/services/rest.service';
import { UserService } from '../core/services/user.service';
import { NotificationService } from '../core/services/notification.service';
import { NotificationModel } from '../shared/models/notification.model';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
})
export class ClientComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = 'no-block';
  toggleOn = true;
  windowWidth: number;
  private notificationObject;
  @ViewChild('searchFriends') search_friends: ElementRef;
  @ViewChild('toggleButton') toggle_button: ElementRef;
  @ViewChild('sideMenu') side_menu: ElementRef;

  public htmlButton: string;

  constructor(
    public menuItems: MenuItems,
    private authService: AuthService,
    private router: Router,
    private rest: RestService,
    private userService: UserService,
    private notificationService: NotificationService
  ) {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    this.htmlButton = '<div class="fixed-button">';
    this.htmlButton +=
      '<a href="https://codedthemes.com/item/mash-able-pro-admin-template/" class="btn btn-primary" aria-hidden="true">';
    this.htmlButton += 'On top</a>';
    this.htmlButton += '</div>';
  }

  ngOnInit() {}

  goToState(asideItem) {
    if (asideItem.name !== 'Account Billing') {
      this.router.navigate([asideItem.state]);
    } else {
      const user = this.userService.getUser();
      if (user && user.customer) {
        this.rest
          .client()
          .recurly({ email: this.userService.getCurrentUserEmail() })
          .subscribe(
            response => {
              const token = response.data.content.hosted_login_token;
              /* const win = window.open(
              `https://royaltie.recurly.com/account/${token}`,
              '_blank'
            );*/

              window.open(
                `https://royaltie.recurly.com/account/${token}`,
                '_self'
              );

              // win.focus();
            },
            error => {
              this.notificationObject = new NotificationModel(
                'Pending Migration!',
                'error'
              );
              this.onToast();
            }
          );
      }
    }
  }

  onClickedOutside(e: Event) {
    if (
      this.windowWidth < 768 &&
      this.toggleOn &&
      this.verticalNavType !== 'offcanvas'
    ) {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  searchFriendList(event) {
    const search = this.search_friends.nativeElement.value.toLowerCase();
    let search_input: string;
    let search_parent: any;
    const friendList = document.querySelectorAll(
      '.userlist-box .media-body .chat-header'
    );
    Array.prototype.forEach.call(friendList, function(elements, index) {
      search_input = elements.innerHTML.toLowerCase();
      search_parent = elements.parentNode.parentNode;
      if (search_input.indexOf(search) !== -1) {
        search_parent.classList.add('show');
        search_parent.classList.remove('hide');
      } else {
        search_parent.classList.add('hide');
        search_parent.classList.remove('show');
      }
    });
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn =
        this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
      this.verticalNavType =
        this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
      this.verticalNavType =
        this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  onMobileMenu() {
    this.isCollapsedMobile =
      this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onScroll(event) {
    this.isScrolled = false;
  }

  onLogout() {
    this.authService.logout();
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
