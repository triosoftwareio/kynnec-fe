import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ClientComponent } from './client.component';
import { AccountBillingComponent } from './components/account-billing/account-billing.component';
import { AffiliateProgramComponent } from './components/affiliate-program/affiliate-program.component';
import { GemsComponent } from './components/gems/gems.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AskQuestionComponent } from './components/ask-question/ask-question.component';

const CLIENT_ROUTES: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: '', redirectTo: 'gems' },
      { path: 'affiliate-program', component: AffiliateProgramComponent },
      { path: 'ask-question', component: AskQuestionComponent },
      { path: 'gems', component: GemsComponent },
      { path: 'landing-page', component: LandingPageComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(CLIENT_ROUTES)],
  exports: [RouterModule],
})
export class ClientRouting {}
