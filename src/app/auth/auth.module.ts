import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { AuthRouting } from './auth.routing';
import { AppSharedModule } from '../shared/app-shared.module';
import { OrderGemsComponent } from '../order/components/order-gems/order-gems.component';
import { TermsOfServiceComponent } from '../order/components/terms-of-service/terms-of-service.component';
import { PrivacyPolicyComponent } from '../order/components/privacy-policy/privacy-policy.component';
import { ThankYouPageComponent } from '../order/components/thank-you-page/thank-you-page.component';
import { RestorePasswordComponent } from './components/restore-password/restore-password.component';

@NgModule({
  imports: [CommonModule, AuthRouting, RouterModule, AppSharedModule],
  declarations: [
    AuthComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RestorePasswordComponent,
  ],
})
export class AuthModule {}
