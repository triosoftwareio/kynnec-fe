import { Component, OnInit } from '@angular/core';
import { AuthBase } from '../../models/auth-base';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { RestorePasswordModel } from '../../models/restore-password.model';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss'],
})
export class RestorePasswordComponent extends AuthBase implements OnInit {
  private restorePasswordModel: RestorePasswordModel;
  isPasswordRestored: boolean;
  isErrorDetected: boolean;
  errorLabel: string;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) {
    super();
    this.form = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });

    this.authService.errorEventEmitter.subscribe(error => {
      this.errorLabel = error;
      this.isErrorDetected = true;
    });

    this.authService.responseStatusEventEmitter.subscribe(status => {
      if (status) {
        this.isErrorDetected = false;
        this.isPasswordRestored = true;
      }
    });
  }

  ngOnInit() {}

  onRestore() {
    this.form.controls['password'].markAsTouched();
    this.form.controls['confirmPassword'].markAsTouched();

    if (
      this.form.valid &&
      this.isPasswordValid() &&
      this.isComparableFieldSame('password', 'confirmPassword')
    ) {
      this.activatedRoute.params.subscribe((params: any) => {
        const restoreHash = params['restoreHash'];
        const password = this.form.controls['password'].value;

        this.restorePasswordModel = new RestorePasswordModel(
          password,
          password,
          restoreHash
        );
        this.authService.restorePassword(this.restorePasswordModel);
      });
    }
  }

  onLogin() {
    this.router.navigate(['auth', 'login']);
  }
}
