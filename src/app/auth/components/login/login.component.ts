import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  invalidEmailMessage = 'Incorrect e-mail';
  invalidPasswordMessage = 'Incorrect password';
  loading: boolean;

  loginErrorMessage: string;
  isErrorPresent: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.authService.errorEventEmitter.subscribe(error => {
      this.loginErrorMessage = error;
      this.isErrorPresent = true;
      this.loading = false;
    });
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  onLogin() {
    this.loginForm.controls['login'].markAsTouched();
    this.loginForm.controls['password'].markAsTouched();

    if (this.loginForm.valid) {
      const login = this.loginForm.controls['login'].value;
      const password = this.loginForm.controls['password'].value;
      this.loading = true;
      this.authService.login(login, password);
    }
  }

  onInput() {
    this.isErrorPresent = false;
  }
}
