import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthBase, isEmailValid } from '../../models/auth-base';
import { RestService } from '../../../core/services/rest.service';
import {
  INVALID_EMAIL_LABEL,
  REQUIRED_EMAIL_LABEL,
} from '../../../shared/constants';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent extends AuthBase implements OnInit {
  invalidEmailLabel = INVALID_EMAIL_LABEL;
  requiredEmailLabel = REQUIRED_EMAIL_LABEL;
  isSubmitPressed: boolean;
  isRestoreEmailSendSuccessful: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    super();

    this.form = this.formBuilder.group({
      emailAddress: ['', [Validators.required, isEmailValid]],
    });

    this.authService.errorEventEmitter.subscribe(error => {
      this.invalidEmailLabel = error;
      this.form.controls['emailAddress'].setErrors({ invalidEmail: true });
    });

    this.authService.responseStatusEventEmitter.subscribe(status => {
      if (status) {
        this.isRestoreEmailSendSuccessful = true;
      }
    });
  }

  resetPasswordClick() {
    const email = this.form.controls['emailAddress'].value;
    this.isSubmitPressed = true;

    if (this.form.valid) {
      this.authService.forgotPassword(email);
    }
  }

  onFieldInput() {
    this.isSubmitPressed = false;
    this.invalidEmailLabel = INVALID_EMAIL_LABEL;
  }

  ngOnInit() {}
}
