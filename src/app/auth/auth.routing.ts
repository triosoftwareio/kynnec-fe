import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { AuthComponent } from './auth.component';
import { OrderGemsComponent } from '../order/components/order-gems/order-gems.component';
import { TermsOfServiceComponent } from '../order/components/terms-of-service/terms-of-service.component';
import { PrivacyPolicyComponent } from '../order/components/privacy-policy/privacy-policy.component';
import { ThankYouPageComponent } from '../order/components/thank-you-page/thank-you-page.component';
import { RestorePasswordComponent } from './components/restore-password/restore-password.component';

const LOGIN_ROUTES: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      { path: '', redirectTo: 'login' },
      { path: 'login', component: LoginComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      {
        path: 'restore-password/:restoreHash',
        component: RestorePasswordComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(LOGIN_ROUTES)],
  exports: [RouterModule],
})
export class AuthRouting {}
