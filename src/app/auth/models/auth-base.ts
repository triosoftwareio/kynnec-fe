import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import {
  EMAIL_REGEXP,
  ENTER_SAME_EMAIL_LABEL,
  ENTER_SAME_PASSWORDS_LABEL,
  ONLY_TEXT_REGEXP,
  PASSWORD_REGEXP,
  REQUIRED_EMAIL_LABEL,
  REQUIRED_PASSWORD_LABEL,
} from '../../shared/constants';

export function isEmailValid(abstractControl: AbstractControl) {
  const field = abstractControl.value;
  const regexp = new RegExp(EMAIL_REGEXP);

  if (regexp.test(field)) {
    return null;
  } else {
    return { invalidEmail: true };
  }
}

export class AuthBase {
  invalidPasswordLabel = REQUIRED_PASSWORD_LABEL;
  invalidEmailAddressLabel = REQUIRED_EMAIL_LABEL;
  form: FormGroup;

  constructor() {}

  public isComparableFieldSame(
    firstControlName: string,
    secondControlName: string
  ): boolean {
    const mainField = this.form.controls[firstControlName].value;
    const confirmMainField = this.form.controls[secondControlName].value;

    if (mainField !== confirmMainField) {
      this.form.controls[firstControlName].setErrors({ incorrect: true });
      this.form.controls[secondControlName].setErrors({ incorrect: true });
      if (firstControlName === 'password') {
        this.invalidPasswordLabel = ENTER_SAME_PASSWORDS_LABEL;
      } else {
        this.invalidEmailAddressLabel = ENTER_SAME_EMAIL_LABEL;
      }

      return false;
    }

    return true;
  }

  public isPasswordValid(): boolean {
    const password = this.form.controls['password'].value;
    const regexp = new RegExp(PASSWORD_REGEXP);

    if (!regexp.test(password)) {
      this.invalidPasswordLabel =
        'Password must have at least 8 symbols, one number and uppercase and lowercase letter';
      this.form.controls['password'].setErrors({ incorrect: true });
      return false;
    }

    this.form.controls['password'].setErrors(null);
    return true;
  }

  public isRepeatedEmailValid(): boolean {
    const email = this.form.controls['emailAddress'].value;
    const regexp = new RegExp(EMAIL_REGEXP);

    if (!regexp.test(email)) {
      if (this.isComparableFieldSame('emailAddress', 'confirmEmailAddress')) {
        this.form.controls['emailAddress'].setErrors({ incorrect: true });
        this.form.controls['confirmEmailAddress'].setErrors({
          incorrect: true,
        });
      } else {
        this.form.controls['emailAddress'].setErrors({ incorrect: true });
      }
      return false;
    }

    this.form.controls['emailAddress'].setErrors(null);
    return true;
  }

  public isSingleEmailValid(): boolean {
    const email = this.form.controls['emailAddress'].value;
    const regexp = new RegExp(EMAIL_REGEXP);

    if (!regexp.test(email)) {
      this.form.controls['emailAddress'].setErrors({ incorrect: true });
      return false;
    } else {
      this.form.controls['emailAddress'].setErrors(null);
      return true;
    }
  }

  public unTouchFields(firstControlName: string, secondControlName: string) {
    this.form.controls[firstControlName].setErrors(null);
    this.form.controls[secondControlName].setErrors(null);
  }
}
