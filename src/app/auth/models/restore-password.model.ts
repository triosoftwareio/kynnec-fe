export class RestorePasswordModel {
  password: string;
  confirmPassword: string;
  restoreHash: string;

  constructor(password: string, confirmPassword: string, restoreHash: string) {
    this.password = password;
    this.confirmPassword = confirmPassword;
    this.restoreHash = restoreHash;
  }
}
