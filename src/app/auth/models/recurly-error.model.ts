export class RecurlyErrorModel {
  code: string;
  fields: string[] = [];
  message: string;
  name: string;
}
