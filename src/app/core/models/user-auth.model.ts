import { CustomerModel } from './customer.model';

export class UserAuthModel {
  token: string;
  customer?: CustomerModel;
  roles: string[];
}
