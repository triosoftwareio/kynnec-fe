export class CustomerModel {
  _id: number;
  mdc_id: number;
  email: string;
  firstName: string;
  lastName: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  country: string;
  contactType: string;
  dateCreated: string;
  emailOpens: number;
  emailClicks: number;
  visits: number;
  views: number;
  homePhone: string;
  tags: string;
  tagIds: number;
  gemOrderStatus: number;
  affiliate_id: string;
  affiliateCode: string;
}
