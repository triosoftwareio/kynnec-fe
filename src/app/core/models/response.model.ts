export interface ResponseModel {
  status: number;
  error: string;
  data: ResponseData;
}

export interface ResponseData {
  content: any;
  message: string;
}
