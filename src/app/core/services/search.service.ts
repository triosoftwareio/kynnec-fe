import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SearchService {
  modelChanged: Subject<string> = new Subject<string>();
  constructor() {}
  changeModel(cb) {
    this.modelChanged
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(model => {
        cb(model);
      });
  }
  nextValue(value) {
    this.modelChanged.next(value);
  }
}
