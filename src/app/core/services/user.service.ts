import { Injectable } from '@angular/core';
import { Customer } from '../../shared/models/customer.model';
import { User } from '../../management/models/user.model';

@Injectable() // service to avoid circular injection between HttpService and AuthService
export class UserService {
  private currentUserEmail: string;
  private user: any;

  constructor() {}

  getUser() {
    this.user = localStorage.getItem('currentUser');
    if (!this.user) {
      return null;
    }

    const parsedUser = JSON.parse(this.user);
    this.currentUserEmail = parsedUser.login;
    return parsedUser;
  }
  saveUser(newUserData) {
    localStorage.removeItem('currentUser');
    localStorage.setItem('currentUser', JSON.stringify(newUserData));
  }

  setCurrentUserEmail(email: string) {
    this.currentUserEmail = email;
  }

  getCurrentUserEmail() {
    return this.currentUserEmail;
  }
}
