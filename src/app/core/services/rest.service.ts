import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthStatusModel } from '../../auth/models/auth-status.model';
import { UserService } from './user.service';
import { BACK_END_SERVER_URL } from '../../../../config';

@Injectable()
export class RestService {
  private prefixRestPath: string = BACK_END_SERVER_URL;
  headers: HttpHeaders;

  constructor(private http: HttpClient, private userService: UserService) {}

  makeRequest(
    method: string,
    restUrl: string,
    body?: RequestOptionsArgs,
    params?: RequestOptionsArgs
  ): Observable<any> {
    const url = this.prefixRestPath + 'api/v1' + restUrl;
    const options: any = {
      headers: this.createHeaders(),
    };
    if (method === 'get') {
      options.params = params;
      return this.http[method](url, options);
    } else {
      return this.http[method](url, body, options);
    }
  }

  createHeaders() {
    const user = this.userService.getUser();
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    if (user) {
      const token = user.token;
      headers = headers.set('Authorization', `Bearer ${token}`);
    }
    return headers;
  }

  makeUrlSearchParams(params) {
    const options: URLSearchParams = new URLSearchParams();
    for (const key of Object.keys(params)) {
      options.set(key, params[key]);
    }
    return options;
  }

  auth() {
    return {
      register: params => this.makeRequest('post', '/auth/register', params),
      login: body => this.makeRequest('post', '/auth/login', body, false),
      logout: () => this.makeRequest('get', '/auth/logout'),
      forgot: params => this.makeRequest('put', '/auth/forgot', params),
      change: params => this.makeRequest('put', '/auth/change', params),
      order: params =>
        this.makeRequest('post', '/recurly/createSubscription', params),
    };
  }

  customers() {
    return {
      list: params =>
        this.makeRequest(
          'get',
          '/operator/customers',
          false,
          this.makeUrlSearchParams(params)
        ),
      impersonate: id =>
        this.makeRequest('get', `/operator/customers/${id}/impersonate`),
    };
  }

  operator() {
    return {
      beacon: {
        list: params =>
          this.makeRequest(
            'get',
            '/operator/beacons',
            false,
            this.makeUrlSearchParams(params)
          ),
        view: id => this.makeRequest('get', `/operator/beacons/${id}`),
        update: (id, attachmentName, body) =>
          this.makeRequest(
            'put',
            `/operator/beacons/${id}/attachment/${attachmentName}`
          ),
        changeStatus: (id, body) =>
          this.makeRequest('put', `/operator/beacon/${id}/deactivate`, body),
        bulkChangeStatus: body =>
          this.makeRequest('put', `/operator/beacons/bulkChangeStatus`, body),
        decommission: (id, body) =>
          this.makeRequest('put', `/operator/beacons/${id}/decommission`, body),
        ordered: params =>
          this.makeRequest(
            'get',
            '/operator/beacons-ordered?skip=0&limit=100000',
            false,
            this.makeUrlSearchParams(params)
          ),
        search: params =>
          this.makeRequest(
            'get',
            `/operator/beacons-search/`,
            false,
            this.makeUrlSearchParams(params)
          ),
        provision: (id, body) =>
          this.makeRequest('put', `/operator/beacons/${id}/provision`, body),
        provisioned: () =>
          this.makeRequest('get', '/operator/beacons-provisioned', false),
        ship: body => this.makeRequest('put', '/operator/beacons-ship', body),
      },
      project: {
        list: params =>
          this.makeRequest(
            'get',
            `/operator/projects-list`,
            false,
            this.makeUrlSearchParams(params)
          ),
        create: accountId =>
          this.makeRequest('post', `/operator/accounts/${accountId}/projects`),
        view: (accountId, projectId) =>
          this.makeRequest(
            'get',
            `/operator/accounts/${accountId}/projects/${projectId}`
          ),
        update: (accountId, projectId) =>
          this.makeRequest(
            'put',
            `/operator/accounts/${accountId}/projects/${projectId}`
          ),
      },
      account: {
        update: body => this.makeRequest('put', `/operator/accounts`, body),
      },
      changeRequest: {
        list: () =>
          this.makeRequest('get', '/operator/beacons/attachmentsRequests'),
        approve: id =>
          this.makeRequest('put', `/operator/beacons/${id}/approveAttachment`),
        decline: (id, body) =>
          this.makeRequest(
            'put',
            `/operator/beacons/${id}/declineAttachment`,
            body
          ),
      },
    };
  }

  commission() {
    return {
      stats: {
        show: body =>
          this.makeRequest('post', '/customer/affiliate/commission', body),
      },
    };
  }

  client() {
    return {
      changeAffiliateCode: body =>
        this.makeRequest('post', '/customer/affiliate/changeCode', body),
      beacon: {
        list: params =>
          this.makeRequest(
            'get',
            '/customer/beacons',
            false,
            this.makeUrlSearchParams(params)
          ),
        update: (id, attachmentName, body) =>
          this.makeRequest(
            'put',
            `/customer/beacons/${id}/attachment/${attachmentName}`,
            body
          ),
      },
      recurly: params =>
        this.makeRequest(
          'get',
          `/recurly/lookUpAccount`,
          false,
          this.makeUrlSearchParams(params)
        ),
    };
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.text() || 'Server Error');
  }

  // getData(restUrl: string, param?: string): Observable<any> {
  //   console.log('GetData: ' + restUrl + (param ? param : ''));
  //   return this.http
  //     .get(this.prefixRestPath + restUrl + (param ? param : ''), {
  //       headers: this.headers,
  //     })
  //     .map((response: Response) => response.json())
  //     .catch(this.handleError);
  // }
  //
  // postData(restUrl: string, data: any): Observable<any> {
  //   console.log('PostData: ' + restUrl);
  //   return this.http.post(restUrl, JSON.stringify(data), {
  //     headers: this.headers,
  //   });
  // }
  //
  // putData(restUrl: string, data: any): Observable<any> {
  //   console.log('PutData: ' + restUrl);
  //   return this.http.put(restUrl, JSON.stringify(data), {
  //     headers: this.headers,
  //   });
  // }
  //
  // deleteData(restUrl: string): Observable<any> {
  //   console.log('Delete: ' + restUrl);
  //   return this.http.delete(restUrl);
  // }
}
