import { Injectable, EventEmitter } from '@angular/core';
import { NotificationModel } from '../../shared/models/notification.model';

@Injectable()
export class NotificationService {
  // toastyParams: any;

  toastyLoaderEventEmmiter = new EventEmitter<NotificationModel>();

  showToasty(notificationModel: NotificationModel) {
    this.toastyLoaderEventEmmiter.emit(notificationModel);
  }

  onInit() {}
}
