import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserAuthModel } from '../models/user-auth.model';
import { CustomerModel } from '../models/customer.model';
import { HttpClient } from '@angular/common/http';
import { RestService } from './rest.service';
import { ResponseModel } from '../models/response.model';
import { AuthStatusModel } from '../../auth/models/auth-status.model';
import { HttpHeaderService } from './http-header.service';
import { UserService } from './user.service';
import { RestorePasswordModel } from '../../auth/models/restore-password.model';

@Injectable()
export class AuthService {
  readonly ADMIN_ROLE = 'ADMIN_ROLE';
  readonly OPERATOR_ROLE = 'OPERATOR_ROLE';
  readonly CUSTOMER_ROLE = 'CUSTOMER_ROLE';

  loginStatus: boolean;
  adminUser: CustomerModel; // mock
  customerUser: CustomerModel; // mock
  adminAuthModel: UserAuthModel; // mock
  customerAuthModel: UserAuthModel; // mock
  currentUser: UserAuthModel;

  errorEventEmitter = new EventEmitter<string>();
  responseStatusEventEmitter = new EventEmitter<boolean>();

  constructor(
    private router: Router,
    private rest: RestService,
    private httpHeaderService: HttpHeaderService,
    private userService: UserService
  ) {
    this.currentUser = userService.getUser();
    this.mockUserData();
  }

  login(login: string, password: string) {
    this.rest
      .auth()
      .login({ login, password })
      .subscribe(
        response => {
          const responseUserModel: UserAuthModel = response.data.content;
          if (response.status === 200 && responseUserModel) {
            localStorage.setItem(
              'currentUser',
              JSON.stringify(responseUserModel)
            );

            if (responseUserModel.roles.includes('CUSTOMER_ROLE')) {
              this.router.navigate(['client']);
            } else if (
              responseUserModel.roles.includes('OPERATOR_ROLE') ||
              responseUserModel.roles.includes('ADMIN_ROLE')
            ) {
              this.router.navigate(['management']);
            }

            this.userService.setCurrentUserEmail(login);
          } else {
            this.router.navigate(['auth']);
          }
        },
        error => {
          this.errorEventEmitter.emit(JSON.parse(error.error).error.message);
        }
      );
  }

  logout(): void {
    this.rest
      .auth()
      .logout()
      .subscribe((responseUserModel: UserAuthModel) => {
        if (responseUserModel) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['auth']);
        }
      });
    /*//  real log out
     this.rest.getData('/api/v1/auth/logout').subscribe((response: ResponseModel) => {
     if (response.status === 200) {
     localStorage.removeItem('currentUser');
     this.httpHeaderService.authorizationEventEmitter.emit(false);
     }
     });*/

    localStorage.removeItem('currentUser'); // mock
    this.httpHeaderService.authorizationEventEmitter.emit(
      new AuthStatusModel(false)
    ); // mock

    // this.currentUserEmail = null;
    this.router.navigateByUrl('/auth');
  }

  forgotPassword(email: string) {
    this.rest
      .auth()
      .forgot({ login: email })
      .subscribe(
        response => {
          this.responseStatusEventEmitter.emit(true);
        },
        error => {
          this.errorEventEmitter.emit(JSON.parse(error.error).error.message);
        }
      );
  }

  restorePassword(restorePasswordModel: RestorePasswordModel) {
    this.rest
      .auth()
      .change(restorePasswordModel)
      .subscribe(
        responce => {
          this.responseStatusEventEmitter.emit(true);
        },
        error => {
          this.errorEventEmitter.emit(JSON.parse(error.error).error.message);
        }
      );
  }

  getLoginStatus() {
    return this.loginStatus;
  }

  getUserRoles() {
    if (this.currentUser != null) {
      return this.currentUser.roles;
    } else {
      return null;
    }
  }

  private mockUserData() {
    this.adminUser = new CustomerModel();
    this.adminUser._id = 345345;
    this.adminUser.address = 'Brovary';
    this.adminUser.firstName = 'Vlad';
    this.adminUser.lastName = 'Nasadiuk';

    this.adminAuthModel = new UserAuthModel();
    this.adminAuthModel.customer = this.adminUser;
    this.adminAuthModel.token = 'testToken213';
    this.adminAuthModel.roles = ['ADMIN_ROLE'];

    this.customerUser = new CustomerModel();
    this.customerUser._id = 345345;
    this.customerUser.address = 'Brovary';
    this.customerUser.firstName = 'Valalya';
    this.customerUser.lastName = 'Kushnir';

    this.customerAuthModel = new UserAuthModel();
    this.customerAuthModel.customer = this.customerUser;
    this.customerAuthModel.token = 'testToken213';
    this.customerAuthModel.roles = ['CUSTOMER_ROLE'];
  }
}
