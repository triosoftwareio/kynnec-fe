import { EventEmitter, Injectable } from '@angular/core';
import { AuthStatusModel } from '../../auth/models/auth-status.model';

@Injectable() // service to avoid circular injection between HttpService and AuthService
export class HttpHeaderService {
  authorizationEventEmitter = new EventEmitter<AuthStatusModel>();

  constructor() {}
}
