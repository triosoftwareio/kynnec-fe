import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found-page',
  templateUrl: './not-found-page.component.html',
  styleUrls: ['./not-found-page.component.scss'],
})
export class NotFoundPageComponent implements OnInit {
  loading: boolean;

  constructor(private router: Router) {}

  ngOnInit() {}

  onOrderClick() {
    this.loading = true;
    this.router.navigate(['order', 'gems']);
  }

  onLoginClick() {
    this.loading = true;
    this.router.navigate(['auth', 'login']);
  }
}
