import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import 'rxjs/add/operator/filter';
import {
  state,
  style,
  transition,
  animate,
  trigger,
  AUTO_STYLE,
} from '@angular/animations';
import { MenuItems } from '../../../shared/components/menu-items/menu-items';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  ngOnInit(): void {}
}
