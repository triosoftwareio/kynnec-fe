import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';
import { AppSharedModule } from '../shared/app-shared.module';
import { GoTopButtonModule } from 'ng2-go-top-button';
import { HttpModule } from '@angular/http';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { RestService } from './services/rest.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpHeaderService } from './services/http-header.service';
import { UserService } from './services/user.service';
import { SearchService } from './services/search.service';
import { NotificationService } from './services/notification.service';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { MomentModule } from 'angular2-moment';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AppSharedModule,
    GoTopButtonModule,
    HttpModule,
    HttpClientModule,
    MomentModule,
  ],
  exports: [RouterModule, AppSharedModule, FormsModule, HttpModule],
  declarations: [HomeComponent, NotFoundPageComponent],
  providers: [
    AuthService,
    AuthGuard,
    RestService,
    HttpHeaderService,
    UserService,
    SearchService,
    NotificationService,
  ],
})
export class CoreModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
