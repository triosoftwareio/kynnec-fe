export class ChargeModel {
  shippingPrice: number;
  lastMonthPayment: number;
  beaconQuantity: number;

  constructor(
    shippingPrice: number,
    lastMonthPayment: number,
    beaconQuantity: number
  ) {
    this.shippingPrice = shippingPrice;
    this.lastMonthPayment = lastMonthPayment;
    this.beaconQuantity = beaconQuantity;
  }
}
