import { ChargeModel } from './charge.model';

export class OrderModel {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  referredBy: string;
  emailAddress: string;
  password: string;
  country: string;
  afiliate: boolean;
  shippingAdress1: string;
  shippingAdress2: string;
  postalCode: string;
  city: string;
  state: string;
  recurlyToken;
  charge: ChargeModel;
}
