import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { OrderGemsComponent } from './components/order-gems/order-gems.component';
import { OrderComponent } from './order.component';
import { ThankYouPageComponent } from './components/thank-you-page/thank-you-page.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';

const ORDER_ROUTES: Routes = [
  {
    path: '',
    component: OrderComponent,
    children: [
      { path: '', redirectTo: 'gems' },
      { path: 'gems', component: OrderGemsComponent },
      { path: 'successful', component: ThankYouPageComponent },
      { path: 'gems/terms-of-service', component: TermsOfServiceComponent },
      { path: 'gems/privacy-policy', component: PrivacyPolicyComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(ORDER_ROUTES)],
  exports: [RouterModule],
})
export class OrderRouting {}
