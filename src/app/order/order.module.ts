import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderGemsComponent } from './components/order-gems/order-gems.component';
import { OrderRouting } from './order.routing';
import { AppSharedModule } from '../shared/app-shared.module';
import { OrderComponent } from './order.component';
import { ThankYouPageComponent } from './components/thank-you-page/thank-you-page.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';

@NgModule({
  imports: [CommonModule, OrderRouting, AppSharedModule],
  declarations: [
    OrderComponent,
    OrderGemsComponent,
    ThankYouPageComponent,
    PrivacyPolicyComponent,
    TermsOfServiceComponent,
  ],
})
export class OrderModule {}
