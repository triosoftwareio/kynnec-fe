import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthBase, isEmailValid } from '../../../auth/models/auth-base';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ACTIVATION_FEE,
  USA_AND_CANADA_BASIC_SHIPPING_FEE,
  INVALID_CARD_NUMBER,
  INVALID_CVV_CODE,
  INVALID_LAST_NAME_FORMAT,
  INVALID_MONTH,
  INVALID_NAME_FORMAT,
  INVALID_POSTAL_CODE,
  INVALID_STATE_FORMAT,
  INVALID_YEAR,
  MONTHLY_SUBSCRIPTION_FEE,
  NAME_AND_LAST_NAME_REGEXP,
  NUMBER_ARE_NOT_ALLOWED_LABEL,
  ONLY_LETTER_REGEXP,
  ONLY_TEXT_REGEXP,
  REQUIRED_ADDRESS_LABEL,
  REQUIRED_CITY_LABEL,
  REQUIRED_COUNTRY_LABEL,
  REQUIRED_FIRST_NAME_LABEL,
  REQUIRED_LAST_NAME_LABEL,
  REQUIRED_NICKNAME_LABEL,
  REQUIRED_PHONE_NUMBER_LABEL,
  REQUIRED_POSTAL_CODE_LABEL,
  REQUIRED_STATE_LABEL,
  OTHER_COUNTRIES_BASIC_SHIPPING_FEE,
  USA_AND_CANADA_PRIMARY_SHIPPING_FEE,
  OTHER_COUNTRIES_PRIMARY_SHIPPING_FEE,
  ADDITIONAL_BEACON_PRICE,
  ORDER_FORM_INVALID_FORM_ERROR,
  PAYMENT_ERROR,
  INVALID_EMAIL_LABEL,
} from '../../../shared/constants';
import { RecurlyErrorModel } from '../../../auth/models/recurly-error.model';
import { RestService } from '../../../core/services/rest.service';
import { ChargeModel } from '../../models/charge.model';
import { OrderModel } from '../../models/order.model';
import { ResponseModel } from '../../../core/models/response.model';
import { RECURLY_PUBLIC_KEY } from '../../../../../config';

declare var recurly: any;
declare var jQuery: any;

// When a customer hits their 'enter' key while in a field
recurly.on('field:submit', function(event) {
  jQuery('form').submit();
});

// A simple error handling function to expose errors to the customer
function error(err) {
  jQuery('#errors').text(
    'The following fields appear to be invalid: ' + err.fields.join(', ')
  );
  jQuery('button').prop('disabled', false);
  jQuery.each(err.fields, function(i, field) {
    jQuery('[data-recurly=' + field + ']').addClass('error');
  });
}

export function isNumberPresent(abstractControl: AbstractControl) {
  const field = abstractControl.value;
  const regexp = new RegExp(ONLY_TEXT_REGEXP);

  if (regexp.test(field)) {
    return null;
  } else {
    return { onlyTextRequredError: true };
  }
}

export function isLetterPresent(abstractControl: AbstractControl) {
  const field = abstractControl.value;
  const regexp = new RegExp(ONLY_LETTER_REGEXP);

  if (regexp.test(field)) {
    return null;
  } else {
    return { onlyTextRequredError: true };
  }
}

/*
* This validator allow common text with spaces and -, but special characters are not allowed
*/
export function commonTextFieldValidator(abstractControl: AbstractControl) {
  const field = abstractControl.value;
  const regexp = new RegExp(NAME_AND_LAST_NAME_REGEXP);

  if (regexp.test(field)) {
    return null;
  } else {
    return { commonTextFormatError: true };
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './order-gems.component.html',
  styleUrls: ['./order-gems.component.scss'],
})
export class OrderGemsComponent extends AuthBase implements OnInit {
  beaconCounter = 1;
  monthlySubscriptionFee = MONTHLY_SUBSCRIPTION_FEE;
  activationFee = ACTIVATION_FEE;
  firstAndLastPayment = this.monthlySubscriptionFee * 2;
  shippingFee = USA_AND_CANADA_BASIC_SHIPPING_FEE;
  totalDueToday = this.activationFee +
    this.shippingFee +
    this.firstAndLastPayment;
  invalidFirstNameLabel = REQUIRED_FIRST_NAME_LABEL;
  invalidLastNameLabel = REQUIRED_LAST_NAME_LABEL;
  invalidPhoneNumberLabel = REQUIRED_PHONE_NUMBER_LABEL;
  invalidCountryLabel = REQUIRED_COUNTRY_LABEL;
  invalidAffiliateNickname = REQUIRED_NICKNAME_LABEL;
  invalidAdress = REQUIRED_ADDRESS_LABEL;
  invalidZipOrPostalCode = REQUIRED_POSTAL_CODE_LABEL;
  invalidCity = REQUIRED_CITY_LABEL;
  invalidState = REQUIRED_STATE_LABEL;
  numbersAreNotAllowed = NUMBER_ARE_NOT_ALLOWED_LABEL;
  isAffiliateMemberNicknameVisible: boolean;
  invalidCardNumber = INVALID_CARD_NUMBER;
  invalidCvvCode = INVALID_CVV_CODE;
  invalidMonth = INVALID_MONTH;
  invalidYear = INVALID_YEAR;
  invalidPostalCode = INVALID_POSTAL_CODE;
  invalidNameFormat = INVALID_NAME_FORMAT;
  invalidLastNameFormat = INVALID_LAST_NAME_FORMAT;
  invalidStateFormat = INVALID_STATE_FORMAT;

  recurlyToken: string;
  isRecurlyFormValid = false;
  recurlyError: RecurlyErrorModel = new RecurlyErrorModel();

  private chargeData: ChargeModel;
  private orderData: OrderModel;

  isSubmitPressed: boolean;

  private recurlyTokenLoadEventEmitter = new EventEmitter<string>();
  private recurlyValidatorEventEmitter = new EventEmitter<boolean>();

  private additionalBeaconPrice = ADDITIONAL_BEACON_PRICE;
  private anotherCountriesBasicShippingFee = OTHER_COUNTRIES_BASIC_SHIPPING_FEE;
  private anotherCountriesPrimaryShippingFee = OTHER_COUNTRIES_PRIMARY_SHIPPING_FEE;
  private usaAndCanadaBasicShippingFee = USA_AND_CANADA_BASIC_SHIPPING_FEE;
  private usaAndCanadaPrimaryShippingFee = USA_AND_CANADA_PRIMARY_SHIPPING_FEE;

  loading = false;
  generalError: boolean;
  generalErrorLabel = ORDER_FORM_INVALID_FORM_ERROR;
  incorrectEmailFormatLabel = INVALID_EMAIL_LABEL;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private restService: RestService,
    private router: Router,
    private cdr: ChangeDetectorRef
  ) {
    super();

    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required, commonTextFieldValidator]],
      lastName: ['', [Validators.required, commonTextFieldValidator]],
      phoneNumber: ['', Validators.required],
      referredBy: '',
      emailAddress: ['', [Validators.required, isEmailValid]],
      confirmEmailAddress: ['', [Validators.required, isEmailValid]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      country: ['US', Validators.required],
      afiliate: false,
      shippingAdress1: ['', Validators.required],
      shippingAdress2: '',
      postalCode: ['', Validators.required],
      city: ['', [Validators.required]],
      state: ['', [Validators.required, commonTextFieldValidator]],
      priorityShipping: '0',
      recurlyFirstName: ['', [Validators.required, commonTextFieldValidator]],
      recurlyLastName: ['', [Validators.required, commonTextFieldValidator]],
      recurlyCountry: ['US', [Validators.required]],
    });

    this.form.controls['referredBy'].disable(true);
  }

  ngOnInit() {
    recurly.configure({
      publicKey: RECURLY_PUBLIC_KEY,
      required: ['cvv', 'postal_code'],
      style: {
        all: {
          fontSize: '1rem',
          fontColor: '#2c0730',
        },
        number: {
          fontColor: '#000000',
          placeholder: {
            content: ' *Credit Card Number',
            color: '#8d8f8e',
          },
        },
        month: {
          placeholder: {
            content: '*Credit Card month',
            color: '#8d8f8e',
          },
        },
        year: {
          placeholder: {
            content: '*Credit Card year',
            color: '#8d8f8e',
          },
        },
        cvv: {
          placeholder: {
            content: '*CVV',
            color: '#8d8f8e',
          },
        },
      },
    });

    // On form submit, we stop submission to go get the token
    const self = this;

    jQuery('form').on('submit', function(event) {
      // Prevent the form from submitting while we retrieve the token from Recurly
      event.preventDefault();

      // Reset the errors display
      jQuery('#errors').text('');
      jQuery('input').removeClass('error');

      // Disable the submit button
      //  jQuery('button').prop('disabled', true);

      const form = this;

      // Now we call recurly.token with the form. It goes to Recurly servers
      // to tokenize the credit card information, then injects the token into the
      // data-recurly="token" field above
      recurly.token(form, function(err, token) {
        // send any errors to the error function below
        if (err) {
          error(err);

          self.recurlyError = err;
          self.isRecurlyFormValid = false;
          self.recurlyValidatorEventEmitter.emit(self.isRecurlyFormValid);
          //  jQuery('button').prop('disabled', false);
          self.cdr.detectChanges();
          // Otherwise we continue with the form submission
        } else {
          // form.submit();

          if (token && token.id) {
            if (self.recurlyToken == null) {
              self.recurlyToken = token.id;
              self.isRecurlyFormValid = true;
              self.recurlyValidatorEventEmitter.emit(self.isRecurlyFormValid);
              self.recurlyTokenLoadEventEmitter.emit(self.recurlyToken);
              //     jQuery('button').prop('disabled', false);
              self.recurlyError.fields = [];
              self.cdr.detectChanges();
            }
          }
        }
      });
    });

    this.activatedRoute.queryParams.subscribe(params => {
      if (params != null) {
        this.form.patchValue({
          referredBy: params['af'],
        });
      }
    });
  }

  orderCounterPlus() {
    this.beaconCounter++;
    this.monthlySubscriptionFee += this.additionalBeaconPrice;

    this.firstAndLastPayment = this.monthlySubscriptionFee * 2;

    this.totalDueToday =
      this.activationFee + this.shippingFee + this.firstAndLastPayment;
  }

  orderCounterMinus() {
    if (this.beaconCounter > 1) {
      this.beaconCounter--;
      this.monthlySubscriptionFee -= this.additionalBeaconPrice;
    }
    this.firstAndLastPayment = this.monthlySubscriptionFee * 2;

    this.totalDueToday =
      this.activationFee + this.shippingFee + this.firstAndLastPayment;
  }

  onOrderDataClick() {
    this.isSubmitPressed = true;

    this.recurlyValidatorEventEmitter.subscribe(validationResult => {
      this.isRecurlyFormValid = validationResult;
      if (
        this.form.valid &&
        this.isPasswordValid() &&
        this.isRepeatedEmailValid() &&
        this.isComparableFieldSame('emailAddress', 'confirmEmailAddress') &&
        this.isComparableFieldSame('password', 'confirmPassword') &&
        this.isRecurlyFormValid
      ) {
        this.generalError = false;
        this.recurlyTokenLoadEventEmitter.subscribe((token: string) => {
          this.sendData(token);
        });
      } else {
        this.generalError = true;
        this.generalErrorLabel = ORDER_FORM_INVALID_FORM_ERROR;
      }
    });

    if (this.recurlyToken != null) {
      this.sendData(this.recurlyToken);
    }
  }

  private sendData(token: string) {
    this.prepareOrderData(token);
    this.loading = true;

    this.restService
      .auth()
      .order(this.orderData)
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.loading = false;
            this.router.navigate(['/', 'order', 'successful']);
          }
        },
        error => {
          this.generalError = true;
          this.loading = false;
          this.generalErrorLabel = PAYMENT_ERROR;
        }
      );
  }

  calculateShippingFee() {
    if (
      this.form.controls['country'].value == 'US' ||
      this.form.controls['country'].value == 'CA'
    ) {
      if (this.form.controls['priorityShipping'].value == 1) {
        this.shippingFee = this.usaAndCanadaPrimaryShippingFee;
      } else {
        this.shippingFee = this.usaAndCanadaBasicShippingFee;
      }
    } else {
      if (this.form.controls['priorityShipping'].value == 1) {
        this.shippingFee = this.anotherCountriesPrimaryShippingFee;
      } else {
        this.shippingFee = this.anotherCountriesBasicShippingFee;
      }
    }
    this.totalDueToday =
      this.activationFee + this.shippingFee + this.firstAndLastPayment;
  }

  duplicateDataToAnotherInput(
    formControlNameSource: string,
    formControlNameTarget: string
  ) {
    if (this.form.controls[formControlNameSource].valid) {
      this.form.controls[formControlNameTarget].setValue(
        this.form.controls[formControlNameSource].value
      );
    } else {
      this.form.controls[formControlNameTarget].setValue('');
    }
  }

  private prepareOrderData(token: string) {
    const lastMonthPayment = this.firstAndLastPayment / 2;
    this.chargeData = new ChargeModel(
      this.shippingFee,
      lastMonthPayment,
      this.beaconCounter
    );

    this.orderData = new OrderModel();
    this.orderData.firstName = this.form.controls['firstName'].value;
    this.orderData.lastName = this.form.controls['lastName'].value;
    this.orderData.phoneNumber = this.form.controls['phoneNumber'].value;
    this.orderData.referredBy = this.form.controls['referredBy'].value;
    this.orderData.emailAddress = this.form.controls['emailAddress'].value;
    this.orderData.password = this.form.controls['password'].value;
    this.orderData.country = this.form.controls['country'].value;
    this.orderData.afiliate = this.form.controls['afiliate'].value;
    this.orderData.shippingAdress1 = this.form.controls[
      'shippingAdress1'
    ].value;
    this.orderData.shippingAdress2 = this.form.controls[
      'shippingAdress2'
    ].value;
    this.orderData.postalCode = this.form.controls['postalCode'].value;
    this.orderData.city = this.form.controls['city'].value;
    this.orderData.state = this.form.controls['state'].value;
    this.orderData.recurlyToken = token;
    this.orderData.charge = this.chargeData;
  }

  onFormChange() {
    this.generalError = false;
  }

  /*
  * Method for preventing auto complete error for email fields
  */
  onOrderEmailInput() {
    if (this.form.controls['emailAddress'].errors != null) {
      if (this.form.controls['emailAddress'].errors.incorrect != null) {
        this.form.controls['emailAddress'].setErrors(null);
        this.form.controls['confirmEmailAddress'].setErrors(null);
      }
    } else if (this.form.controls['confirmEmailAddress'].errors != null) {
      if (this.form.controls['confirmEmailAddress'].errors.incorrect != null) {
        this.form.controls['emailAddress'].setErrors(null);
        this.form.controls['confirmEmailAddress'].setErrors(null);
      }
    }
  }
}
