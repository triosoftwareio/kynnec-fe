import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BeaconSearchComponent } from './components/beacon-search/beacon-search.component';
import { EditBeaconComponent } from './components/edit-beacon/edit-beacon.component';
import { BeaconHistoryComponent } from './components/beacon-history/beacon-history.component';
import { BeaconActivationComponent } from './components/beacon-activation/beacon-activation.component';
import { ChangeBeaconMessageComponent } from './components/change-beacon-message/change-beacon-message.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { AdjustmentsListComponent } from './components/adjustments-list/adjustments-list.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { AllUsersListComponent } from './components/all-users-list/all-users-list.component';
import { ActivityPageComponent } from './components/activity-page/activity-page.component';
import { ManagementComponent } from './management.component';
import { ManagementRouting } from './management.routing';
import { AppSharedModule } from '../shared/app-shared.module';
import { ClientBeaconRightBlockComponent } from './shared/components/client-beacon-rigth-block/client-beacon-right-block.component';
import { BeaconShipmentComponent } from './components/beacon-shipment/beacon-shipment.component';

@NgModule({
  imports: [CommonModule, RouterModule, ManagementRouting, AppSharedModule],
  declarations: [
    BeaconSearchComponent,
    EditBeaconComponent,
    BeaconHistoryComponent,
    BeaconActivationComponent,
    ChangeBeaconMessageComponent,
    ProjectsListComponent,
    CreateProjectComponent,
    EditProjectComponent,
    AllUsersListComponent,
    ActivityPageComponent,
    ManagementComponent,
    ClientBeaconRightBlockComponent,
    BeaconShipmentComponent,
    UsersListComponent,
    AdjustmentsListComponent,
  ],
})
export class ManagementModule {}
