export interface SubscriptionAddOnsModel {
  code: string;
  price: number;
  quantity: number;
}

export interface SubscriptionModel {
  currency: string;
  id: string;
  periodEnd: string;
  periodStart: string;
  price: number;
  quantity: number;
  state: string;
  subscription_add_ons: SubscriptionAddOnsModel;
}
