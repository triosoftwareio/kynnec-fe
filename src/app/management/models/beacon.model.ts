import { Attachments } from '../../shared/models/attachments.model';
import { Customer } from '../../shared/models/customer.model';
import { Project } from '../../shared/models/project.model';

export interface Beacon {
  _id: string;
  createdOn: string;
  createdBy: string;
  updatedOn: string;
  updatedBy: string;
  beaconName?: string;
  customer: Customer;
  project: Project;
  attachments: Attachments[];
  attachMentsRequest: Attachments[];
  status: string;
  orderStatus: string;
  description: string;
}
