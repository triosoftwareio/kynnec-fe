import { SubscriptionModel } from './subscription.model';

export interface ActivateBeaconCustomerModel {
  affiliate_id: string;
  country: string;
  dateCreated: string;
  firstName: string;
  gemOrderStatus: number;
  lastName: string;
  phoneNumber: string;
  subscription: SubscriptionModel;
  unassignedBeacons: number;
  unassignedBeaconsArray: any[];
  __v: number;
  _id: string;
}
