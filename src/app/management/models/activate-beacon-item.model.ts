export class ActivateBeaconItemModel {
  index: number;
  customerId: string;

  constructor(index: number, customerId: string) {
    this.index = index;
    this.customerId = customerId;
  }
}
