import { Account } from '../../shared/models/account.model';

export interface Project {
  _id: string;
  account: Account;
  numberOfBeacons: number;
  purpose: string;
  status: string;
}
