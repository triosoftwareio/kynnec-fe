import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManagementComponent } from './management.component';
import { ActivityPageComponent } from './components/activity-page/activity-page.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { AdjustmentsListComponent } from './components/adjustments-list/adjustments-list.component';
import { BeaconActivationComponent } from './components/beacon-activation/beacon-activation.component';
import { BeaconSearchComponent } from './components/beacon-search/beacon-search.component';
import { ChangeBeaconMessageComponent } from './components/change-beacon-message/change-beacon-message.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { EditBeaconComponent } from './components/edit-beacon/edit-beacon.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { BeaconHistoryComponent } from './components/beacon-history/beacon-history.component';
import { BeaconShipmentComponent } from './components/beacon-shipment/beacon-shipment.component';

const MANAGEMENT_ROUTES: Routes = [
  {
    path: '',
    component: ManagementComponent,
    children: [
      { path: '', redirectTo: 'beacon-search' },
      { path: 'activity-page', component: ActivityPageComponent },
      { path: 'users-list', component: UsersListComponent },
      { path: 'adjustments-list', component: AdjustmentsListComponent },
      { path: 'beacon-activation', component: BeaconActivationComponent },
      { path: 'beacon-history', component: BeaconHistoryComponent },
      { path: 'beacon-search', component: BeaconSearchComponent },
      {
        path: 'beacon-change-message',
        component: ChangeBeaconMessageComponent,
      },
      { path: 'create-project', component: CreateProjectComponent },
      { path: 'edit-beacon/:id', component: EditBeaconComponent },
      { path: 'edit-project', component: EditProjectComponent },
      { path: 'projects-list', component: ProjectsListComponent },
      { path: 'beacon-shipment', component: BeaconShipmentComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(MANAGEMENT_ROUTES)],
  exports: [RouterModule],
})
export class ManagementRouting {}
