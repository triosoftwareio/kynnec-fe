import { Component, Input, OnInit } from '@angular/core';
import { Beacon } from '../../../../shared/models/beacon.model';

@Component({
  selector: 'app-client-beacon-right-block',
  templateUrl: './client-beacon-right-block.component.html',
  styleUrls: ['./client-beacon-right-block.component.scss'],
})
export class ClientBeaconRightBlockComponent implements OnInit {
  @Input() beacon: Beacon;

  constructor() {}

  ngOnInit() {}

  parseDate(date) {
    const d = new Date(date);
    let dd: any = d.getDate();
    let mm: any = d.getMonth() + 1;
    const yy = d.getFullYear();
    if (dd < 10) {
      dd = `0${dd}`;
    }
    if (mm < 10) {
      mm = `0${mm}`;
    }
    return `${dd}-${mm}-${yy}`;
  }
}
