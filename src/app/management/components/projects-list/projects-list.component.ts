import { Component, ViewChild, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';
import { SearchParams } from '../../../shared/models/search-params.model';
import { Project } from '../../models/project.model';
import { SearchService } from '../../../core/services/search.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

interface Errors {
  email: string;
  password: string;
}

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss'],
})
export class ProjectsListComponent implements OnInit {
  loading: boolean;
  searchModel: string;
  searchParams: SearchParams = {
    keyword: '',
    sort: '_id',
    skip: 0,
    limit: 200,
  };
  projects: Project[];
  accountPopup = false;
  email = '';
  password = '';
  errors: Errors = {
    email: '',
    password: '',
  };
  submit = false;
  limit = 10;
  count = 0;
  offset = 0;
  reorderable = true;
  columns = [];
  private notificationObject;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  constructor(
    private rest: RestService,
    private searchService: SearchService,
    private notificationService: NotificationService
  ) {
    this.loading = false;
    searchService.changeModel(model => {
      this.offset = 0;
      this.searchParams.skip = 0;
      this.searchParams.keyword = model;
      this.getProjects();
    });
  }
  ngOnInit() {
    this.getProjects();
  }
  getProjects() {
    this.loading = true;
    this.rest
      .operator()
      .project.list(this.searchParams)
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.count = response.data.content.recordsTotal;
            this.projects = response.data.content.data;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
          this.count = 0;
          this.projects = [];
        }
      );
  }
  search(text: string) {
    this.searchService.nextValue(text);
  }
  onPage(e) {
    // this.searchParams.skip = e.offset;
    // this.getProjects();
  }
  showAccountPopup(account) {
    this.accountPopup = true;
    this.email = account;
    this.password = '';
  }
  closeAccountPopup() {
    this.accountPopup = false;
    this.email = '';
    this.password = '';
    this.submit = false;
  }
  inputChange(field) {
    this.errors[field] = '';
  }
  accountFormValidation() {
    if (!this.email) {
      this.errors.email = 'Require field';
    }
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.email && !re.test(this.email)) {
      this.errors.email = `Email doesn't valid`;
    }
    if (!this.password) {
      this.errors.password = 'Require field';
    }
    if (!this.email || !this.password) {
      return false;
    }
    return true;
  }
  editAccount() {
    this.submit = true;
    if (!this.accountFormValidation()) {
      return;
    }
    this.loading = true;
    this.rest
      .operator()
      .account.update({ email: this.email, password: this.password })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Account updated',
              'success'
            );
            this.onToast();
            this.projects = response.data.content;
            this.accountPopup = false;
            this.submit = false;
          } else {
            this.notificationObject = new NotificationModel(
              'Something gone wrong',
              'error'
            );
            this.onToast();
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  showProjectPopup() {}
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
