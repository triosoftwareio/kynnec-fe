import { Component, ViewChild, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';
import { Router } from '@angular/router';
import { Beacon } from '../../../shared/models/beacon.model';
import { SearchParams } from '../../../shared/models/search-params.model';
import { SearchService } from '../../../core/services/search.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-beacon-search',
  templateUrl: './beacon-search.component.html',
  styleUrls: ['./beacon-search.component.scss'],
})
export class BeaconSearchComponent implements OnInit {
  loading: boolean;
  searchModel: string;
  searchParams: SearchParams = {
    keyword: '',
    skip: 0,
    limit: 200,
    sort: '_id',
  };
  beacons: Beacon[];
  chosenBeacons: Beacon[] = [];
  status = 'active';
  selectAll = false;
  elements: any;
  reorderable = true;
  columns = [];
  limit = 10;
  count = 0;
  offset = 0;
  allRowsSelected: false;
  private notificationObject;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private rest: RestService,
    private router: Router,
    private searchService: SearchService,
    private notificationService: NotificationService
  ) {
    this.loading = false;
    searchService.changeModel(model => {
      this.offset = 0;
      this.searchParams.skip = 0;
      this.searchParams.keyword = model;
      this.getBeacons();
    });
  }

  ngOnInit() {
    this.getBeacons();
  }
  getBeacons() {
    this.loading = true;
    this.rest
      .operator()
      .beacon.list(this.searchParams)
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.loading = false;
            this.count = response.data.content.recordsTotal;
            this.beacons = response.data.content.data.map(item => {
              const i = item;
              if (item.customer) {
                item.customer.name =
                  item.customer.firstName + ' ' + item.customer.lastName;
              } else {
                i.name = '-';
              }
              return i;
            });
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
          this.beacons = [];
        }
      );
  }
  onPage(e) {
    // this.searchParams.skip = e.offset;
    // this.getBeacons();
  }
  onSelect({ selected }) {
    this.chosenBeacons.splice(0, this.chosenBeacons.length);
    this.chosenBeacons.push(...selected);

    const status = this.chosenBeacons.every(item => item.status === 'INACTIVE');
    if (status) {
      this.status = 'active';
    } else {
      this.status = 'inactive';
    }
  }
  returnOrderStatus(status) {
    const statuses = {
      NEW: 'New',
      ORDERED: 'Ordered',
      SHIPPED: 'Shipped',
      PROVISIONED: 'Provisioned',
    };
    return statuses[status];
  }

  search(text: string) {
    this.searchService.nextValue(text);
  }
  goToEditBeacon(id) {
    this.router.navigateByUrl(`/management/edit-beacon/${id}`);
  }

  selectAllItems() {
    this.selectAll = !this.selectAll;
    this.elements = document.getElementsByClassName('beacon-checkbox');
    for (let i = 0; i < this.elements.length; i++) {
      if (this.selectAll) {
        this.elements[i].checked = true;

        this.chosenBeacons = this.beacons;
        const status = this.chosenBeacons.every(
          item => item.status === 'inactive'
        );
        if (status) {
          this.status = 'active';
        } else {
          this.status = 'inactive';
        }
      } else {
        this.chosenBeacons = [];
        this.elements[i].checked = false;
      }
    }
  }

  changeStatus() {
    this.loading = true;
    this.rest
      .operator()
      .beacon.bulkChangeStatus({
        beacons: this.chosenBeacons,
        status: this.status,
      })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.getBeacons();
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
