import { Component, ViewChild, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';
import { User } from '../../models/user.model';
import { SearchParams } from '../../../shared/models/search-params.model';
import { SearchService } from '../../../core/services/search.service';
import { Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  loading: boolean;
  searchModel: string;
  searchParams: SearchParams = {
    keyword: '',
    sort: '_id',
    skip: 0,
    limit: 200,
  };
  users: User[];
  loadingIndicator = true;
  reorderable = true;
  columns = [{ prop: 'name' }, { prop: 'email' }, { name: 'Actions' }];
  private notificationObject;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  constructor(
    private rest: RestService,
    private searchService: SearchService,
    private router: Router,
    private notificationService: NotificationService
  ) {
    this.loading = false;
    searchService.changeModel(model => {
      this.searchParams.keyword = model;
      this.getUsers();
    });
  }
  ngOnInit() {
    this.getUsers();
  }
  getUsers() {
    this.loading = true;
    this.rest
      .customers()
      .list(this.searchParams)
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.loading = false;
            this.users = response.data.content.data.map(item => {
              const i = item;
              item.name = item.firstName + ' ' + item.lastName;
              return i;
            });
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
          this.users = [];
        }
      );
  }
  search(text: string) {
    this.searchService.nextValue(text);
  }
  impersonate(id) {
    this.loading = true;
    this.rest
      .customers()
      .impersonate(id)
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.loading = false;

            this.rest
              .auth()
              .logout()
              .subscribe(
                (resp: ResponseModel) => {
                  if (resp.status === 200) {
                    localStorage.removeItem('currentUser');
                    localStorage.setItem(
                      'currentUser',
                      JSON.stringify(response.data.content)
                    );
                    this.router.navigate(['client']);
                  }
                },
                error => {
                  this.loading = false;
                  this.notificationObject = new NotificationModel(
                    'Something gone wrong',
                    'error'
                  );
                  this.onToast();
                }
              );
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
