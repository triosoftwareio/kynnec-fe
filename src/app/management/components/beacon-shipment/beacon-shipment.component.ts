import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ActivateBeaconCustomerModel } from '../../models/activate-beacon-customer.model';
import { RestService } from '../../../core/services/rest.service';
import { Params } from '../../../shared/models/params.model';
import { ResponseModel } from '../../../core/models/response.model';
import { Beacon } from '../../../shared/models/beacon.model';
import { Customer } from '../../../shared/models/customer.model';
import { User } from '../../models/user.model';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-beacon-shipment',
  templateUrl: './beacon-shipment.component.html',
  styleUrls: ['./beacon-shipment.component.scss'],
})
export class BeaconShipmentComponent implements OnInit {
  searchModel: string;
  modelChanged: Subject<string> = new Subject<string>();
  searchParams: Params = { keyword: '' };

  provisionedBeacons: Beacon[];
  customerProvisionedBeacons: Beacon[];
  customerProvisionedBeaconsId: string[] = [];
  customers: Customer[] = [];
  selectedCustomer: Customer;

  address: string;
  city: string;
  state: string;
  country: string;
  zip: string;
  loading: boolean;

  private notificationObject;

  constructor(
    private rest: RestService,
    private notificationService: NotificationService
  ) {
    this.modelChanged
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(model => {
        this.searchParams.keyword = model;
        this.getClientAndProvisionedBeacons();
      });
  }

  getClientAndProvisionedBeacons() {
    this.loading = true;

    var customersIdStore = [];
    this.customers = [];

    this.rest
      .operator()
      .beacon.provisioned()
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.provisionedBeacons = response.data.content;

            for (let beacon of this.provisionedBeacons) {
              if (!customersIdStore.includes(beacon.customer._id)) {
                this.customers.push(beacon.customer);
                customersIdStore.push(beacon.customer._id);
              }
            }
          }

          if (this.customers != null) {
            this.selectedCustomer = this.customers[0];
          }

          this.getProvisionedBeaconForCustomer();

          this.loading = false;
        },
        error => {
          this.notificationObject = new NotificationModel(
            'Error occurred!',
            'error'
          );
          this.onToast();
          this.loading = false;
        }
      );
  }

  private getProvisionedBeaconForCustomer() {
    this.customerProvisionedBeacons = [];
    this.customerProvisionedBeaconsId = [];

    this.provisionedBeacons.forEach((provisionedBeacon: Beacon) => {
      if (provisionedBeacon.customer._id === this.selectedCustomer._id) {
        this.customerProvisionedBeacons.push(provisionedBeacon);
        this.customerProvisionedBeaconsId.push(provisionedBeacon._id);
      }
    });
  }

  onCustomerSelect(selectedCustomer: Customer) {
    this.loading = true;
    this.selectedCustomer = selectedCustomer;
    this.getProvisionedBeaconForCustomer();
    this.loading = false;
  }

  ngOnInit() {
    this.getClientAndProvisionedBeacons();
  }

  search(text: string) {
    this.modelChanged.next(text);
  }

  onShipClick() {
    this.loading = true;

    let notes = '';

    if (this.address != null) {
      notes += 'Corrected address: ' + this.address + ' ';
    }

    if (this.city != null) {
      notes += 'Corrected city: ' + this.city + ' ';
    }

    if (this.state != null) {
      notes += 'Corrected state: ' + this.state + ' ';
    }

    if (this.country != null) {
      notes += 'Corrected country: ' + this.country + ' ';
    }

    if (this.zip != null) {
      notes += 'Corrected zip: ' + this.zip + ' ';
    }

    this.rest
      .operator()
      .beacon.ship({
        customerId: this.selectedCustomer._id,
        notes: notes,
        beaconsToShip: this.customerProvisionedBeaconsId,
      })
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              response.data.message,
              'success'
            );
            this.onToast();
            this.getClientAndProvisionedBeacons();
            this.loading = false;
          }
        },
        error => {
          this.notificationObject = new NotificationModel(
            'Shipment error occurred!',
            'error'
          );
          this.onToast();
          this.loading = false;
        }
      );
  }

  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
