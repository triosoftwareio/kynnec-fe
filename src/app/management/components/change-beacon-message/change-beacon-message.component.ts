import { Component, OnInit } from '@angular/core';
import { ResponseModel } from '../../../core/models/response.model';
import { ChangeRequest } from '../../models/change-request.model';
import { RestService } from '../../../core/services/rest.service';
import { SearchService } from '../../../core/services/search.service';
import { Beacon } from '../../../shared/models/beacon.model';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

interface Params {
  keyword: string;
}

@Component({
  selector: 'app-change-beacon-message',
  templateUrl: './change-beacon-message.component.html',
  styleUrls: ['./change-beacon-message.component.scss'],
})
export class ChangeBeaconMessageComponent implements OnInit {
  loading: boolean;
  searchModel: string;
  searchParams: Params = { keyword: '' };
  changeRequests: Beacon[] = [];
  chosenChangeRequest: Beacon = null;
  weekDays: string[] = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];
  declineSubmit = false;
  declineReason: '';
  private notificationObject;
  constructor(
    private rest: RestService,
    private searchService: SearchService,
    private notificationService: NotificationService
  ) {
    this.loading = false;
    searchService.changeModel(model => {
      this.searchParams.keyword = model;
      this.getChangeRequests();
    });
  }

  ngOnInit() {
    this.getChangeRequests();
  }
  getChangeRequests() {
    this.loading = true;
    this.rest
      .operator()
      .changeRequest.list()
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.changeRequests = response.data.content;
            if (this.changeRequests.length) {
              this.chosenChangeRequest = this.changeRequests[0];
            } else {
              this.chosenChangeRequest = null;
            }
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
          this.chosenChangeRequest = null;
          this.changeRequests;
        }
      );
  }
  search(text: string) {
    this.searchService.nextValue(text);
  }
  changeChosenChangeRequest(i) {
    if (this.changeRequests[i]._id) {
      this.chosenChangeRequest = this.changeRequests[i];
    }
  }
  returnBeaconName(name, title) {
    if (title) {
      return `Change for Beacon ${name.slice(-4)}`;
    }
    return name.slice(-4);
  }
  changeInputValue(e) {
    this.declineSubmit = false;
    this.declineReason = e.target.value;
  }
  approveChangeRequest() {
    this.loading = true;
    this.rest
      .operator()
      .changeRequest.approve(this.chosenChangeRequest._id)
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Change request approved',
              'success'
            );
            this.onToast();
            this.getChangeRequests();
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  declineChangeRequest() {
    this.declineSubmit = true;
    if (!this.declineReason) {
      return;
    }
    this.loading = true;
    this.rest
      .operator()
      .changeRequest.decline(this.chosenChangeRequest._id, {
        reason: this.declineReason,
      })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Change request declined',
              'success'
            );
            this.onToast();
            this.declineSubmit = true;
            this.getChangeRequests();
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
