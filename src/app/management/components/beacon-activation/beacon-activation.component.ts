import { Component, OnInit } from '@angular/core';
import { ResponseModel } from '../../../core/models/response.model';
import { Params } from '../../../shared/models/params.model';
import { Subject } from 'rxjs/Subject';
import { RestService } from '../../../core/services/rest.service';
import { ChosenGem } from '../../../shared/models/chosen-gems.model';
import { CustomerModel } from '../../../core/models/customer.model';
import { ActivateBeaconCustomerModel } from '../../models/activate-beacon-customer.model';
import { ActivateBeaconItemModel } from '../../models/activate-beacon-item.model';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-beacon-activation',
  templateUrl: './beacon-activation.component.html',
  styleUrls: ['./beacon-activation.component.scss'],
})
export class BeaconActivationComponent implements OnInit {
  searchModel: string;
  modelChanged: Subject<string> = new Subject<string>();
  searchParams: Params = { keyword: '' };
  gems: ChosenGem[];
  chosenGem: ChosenGem = null;
  selectedIndex: number;
  private customerId: string;
  beaconIdValue = '';
  isProvisionEnable: boolean;
  loading: boolean;
  private notificationObject;

  customers: ActivateBeaconCustomerModel[];

  constructor(
    private rest: RestService,
    private notificationService: NotificationService
  ) {
    this.modelChanged
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(model => {
        this.searchParams.keyword = model;
        this.getClientUnassignBeacons();
      });
  }

  ngOnInit() {
    this.getClientUnassignBeacons();
  }

  getClientUnassignBeacons() {
    this.loading = true;
    this.rest
      .operator()
      .beacon.ordered(this.searchParams)
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.customers = response.data.content.data;

            let i = 0;
            for (const customer of this.customers) {
              customer.unassignedBeaconsArray = new Array(
                customer.unassignedBeacons
              );
              this.customerId = customer._id;

              for (let j = 0; j < customer.unassignedBeaconsArray.length; j++) {
                customer.unassignedBeaconsArray[
                  j
                ] = new ActivateBeaconItemModel(i, this.customerId);
                i++;
              }
            }
          }

          if (this.customers != null && this.customers[0] != null) {
            this.selectedIndex = 0;
            this.customerId = this.customers[0].unassignedBeaconsArray[0].customerId;
          }

          this.loading = false;
        },
        error => {
          this.notificationObject = new NotificationModel(
            'Error occurred!',
            'error'
          );
          this.onToast();
          this.loading = false;
        }
      );
  }

  search(text: string) {
    this.modelChanged.next(text);
  }

  onActivateBeacon(innerListItem: ActivateBeaconItemModel) {
    this.selectedIndex = innerListItem.index;
    this.customerId = innerListItem.customerId;
  }

  onFind() {
    this.loading = true;

    this.rest
      .operator()
      .beacon.search({ beaconName: this.beaconIdValue })
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.chosenGem = response.data.content;

            if (this.chosenGem.customer != null) {
              this.notificationObject = new NotificationModel(
                'This gem has been already activated!',
                'warning'
              );
              this.onToast();
            }
          }

          this.loading = false;
        },
        error => {
          this.chosenGem = null;
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Beacons with id < ' + this.beaconIdValue + ' > doesn`t exist!',
            'error'
          );
          this.onToast();
        }
      );
  }

  onProvisionClick() {
    this.loading = true;

    this.rest
      .operator()
      .beacon.provision(this.chosenGem._id, { customerId: this.customerId })
      .subscribe(
        (response: ResponseModel) => {
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Beacon successfully provisioned!',
              'success'
            );
            this.onToast();

            this.getClientUnassignBeacons();
            this.beaconIdValue = '';
            this.chosenGem = null;
          }
          this.loading = false;
        },
        error => {
          this.notificationObject = new NotificationModel(
            'Error received. Please, try to assign another beacon id',
            'error'
          );
          this.onToast();
          this.getClientUnassignBeacons();
          this.beaconIdValue = '';
          this.loading = false;
        }
      );
  }

  onIdEntered() {
    this.chosenGem = null;
  }

  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
