import { Component, OnInit } from '@angular/core';
import { RestService } from '../../../core/services/rest.service';
import { ResponseModel } from '../../../core/models/response.model';
import { Beacon } from '../../../shared/models/beacon.model';
import { NotificationService } from '../../../core/services/notification.service';
import { NotificationModel } from '../../../shared/models/notification.model';

@Component({
  selector: 'app-edit-beacon',
  templateUrl: './edit-beacon.component.html',
  styleUrls: ['./edit-beacon.component.scss'],
})
export class EditBeaconComponent implements OnInit {
  loading: boolean;
  activeTab = 'beacon';
  beacon: Beacon = null;
  weekDays: string[] = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ];
  submit = false;
  urlRule = /^(https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  urlError = null;
  private notificationObject;
  constructor(
    private rest: RestService,
    private notificationService: NotificationService
  ) {
    this.loading = false;
  }

  ngOnInit() {
    const arr = window.location.pathname.split('/');
    const id = arr[arr.length - 1];
    this.getBeacon(id);
  }
  getBeacon(id) {
    this.loading = true;
    this.rest
      .operator()
      .beacon.view(id)
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.beacon = response.data.content;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  getHistory() {}
  changeActiveTab(tab) {
    this.activeTab = tab;
    if (tab === 'history') {
      this.getHistory();
    }
  }
  onChangeInputData(data): void {
    if (data.field !== 'checkbox') {
      if (data.field === 'url') {
        this.submit = false;
        this.urlError = null;
      }
      this.beacon.attachments[0].notificationMessage[data.field] =
        data.e.target.value;
    } else {
      this.beacon.attachments[0].notificationMessage.days[data.cbIndex] =
        data.e.target.checked;
    }
  }
  returnBeaconName(name) {
    if (!name) {
      return '';
    }
    return `Beacon ${name.slice(-4)}`;
  }
  saveBeacon() {
    this.submit = true;
    const url = this.beacon.attachments[0].notificationMessage.url;
    if (!this.urlRule.test(url)) {
      this.urlError = `Url is not valid`;
      return;
    }
    const arr = this.beacon.attachments[0].attachmentName.split('/');
    const attachmentName = arr[arr.length - 1];
    this.loading = true;
    this.rest
      .operator()
      .beacon.update(this.beacon._id, attachmentName, {
        attachment: this.beacon.attachments[0].notificationMessage,
      })
      .subscribe(
        (response: ResponseModel) => {
          this.submit = false;
          this.notificationObject = new NotificationModel(
            'Beacon saved',
            'success'
          );
          this.onToast();
          this.loading = false;
          if (response.status === 200) {
            this.beacon = response.data.content;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  changeBeaconStatus() {
    const beaconId = this.beacon._id;
    const beaconName = this.beacon.beaconName.split('/')[1];
    const projectId = this.beacon.project.projectId;
    this.loading = true;
    this.rest
      .operator()
      .beacon.changeStatus(beaconId, { beaconName, projectId })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.notificationObject = new NotificationModel(
              'Beacon status changed',
              'success'
            );
            this.onToast();
            this.beacon = response.data.content;
          }
        },
        error => {
          this.loading = false;
          this.notificationObject = new NotificationModel(
            'Something gone wrong',
            'error'
          );
          this.onToast();
        }
      );
  }
  decommissionBeacon() {
    const beaconId = this.beacon._id;
    const beaconName = this.beacon.beaconName.split('/')[1];
    const projectId = this.beacon.project.projectId;
    this.loading = true;
    this.rest
      .operator()
      .beacon.decommission(beaconId, { beaconName, projectId })
      .subscribe(
        (response: ResponseModel) => {
          this.loading = false;
          if (response.status === 200) {
            this.beacon = response.data.content;
          }
        },
        error => {
          this.loading = false;
        }
      );
  }
  onToast() {
    this.notificationService.showToasty(this.notificationObject);
  }
}
